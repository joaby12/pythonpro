# pythonpro
Módulo para exemplificar construção de projetos Python no curso PyTools.

Nesse cursos e ensinado como contribuir com Projetos de código Aberto

Suportada versão 3 de Python

Para instalar:
```Console
python3 -m venv .venv
source .vev/bin/activate
pip install -r requirements-dev.txt

```

Para conferir qualidade do código:

```console
flake8
```